<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('categorys.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Category::create([
        //     'name' => $request->name
        // ]);
        //验证数据
        $request->validate([
            'name' => 'required|unique:categories|max:255'
        ]);

        //插入数据
        $node = Category::create([
            'name' => $request->name
        ]);

        if($request->parentId){
            $node->parent_id = $request->parentId;
            $node->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->update([
            'name'=>$request->name
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
    }

    public function insertBefore(Request $request, Category $category)
    {
        //验证数据
        $request->validate([
            'name' => 'required|unique:categories|max:255'
        ]);

        //插入数据
        $node = Category::create([
            'name' => $request->name
        ]);

        $node->insertBeforeNode($category);
    }

    public function insertAfter(Request $request, Category $category)
    {
        //验证数据
        $request->validate([
            'name' => 'required|unique:categories|max:255'
        ]);

        //插入数据
        $node = Category::create([
            'name' => $request->name
        ]);

        $node->insertAfterNode($category);
    }

    public function moveUp(Category $category)
    {
        //判断如果没有前面的兄弟节点就直接返回true
        if(!$category->getPrevSibling()){
            return response('已经是顶级分类，不能再继续上移', 200);
        }

        $category->up();
    }

    public function moveDown(Category $category)
    {
        //判断如果没有后面的兄弟节点就直接返回true
        if(!$category->getNextSibling()){
            return response('已经是末级分类，不能再继续下移', 200);
        }

        $category->down();
    }

    public function getCategories()
    {
        return Category::defaultOrder()->get()->toTree();
    }

    public function submitCate(Request $request)
    {
        Category::rebuildTree($request->categories, true);
    }
}
