import Category from './components/Category.vue';
import {EventBus} from './components/event-bus.js';
import draggable from 'vuedraggable';


var vm = new Vue({
    el: '#app',

    components: {
        'category': Category,
        'draggable': draggable
    },

    data: {
        categories:[],
        newCategory:''
    },

    created : function(){
        this.getCategories();
        EventBus.$on('getCategories', this.getCategories);
    },

    methods: {
        getCategories: function(){
            axios.get('category/getCategories').then((response)=>{
                this.categories = response.data;
            }).catch((error)=>{
                throw error;
            });
        },

        addCategory: function(){
            // axios.post('category', {'name':this.newCategory}).then((response)=>{
            //     this.getCategories();
            //     this.newCategory = '';
            // }).catch((error)=>{
            //     throw error;
            // });

            this.categories.push({parent_id:null, name:this.newCategory, children:[]});
            this.newCategory="";

        },

        submitCate: function(){
            axios.post('category/submitCate', {categories:this.categories}).then((response)=>{

            }).catch((error)=>{
                throw error;
            });
        }
    }
})
