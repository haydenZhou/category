@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    添加根分类
                </div>

                <div class="panel-body">
                    <input type="text" class="form-control" v-model="newCategory" @keyup.enter="addCategory">
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button type="button" @click="submitCate">提交分类</button>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <ul class="list-group">
                {{-- <li class="list-group-item" v-for="category in categories" :key="category.id">@{{category.name}}</li> --}}

                <draggable v-model="categories" :options="{draggable: '.item', group:'list'}">
                    <category v-for="category in categories" :key="category.id" :category="category" :pcategories=categories></category>
                </draggable>
            </ul>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="js/categoryTree.js"></script>
@endsection
