<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('category/submitCate', 'CategoryController@submitCate');
Route::get('category/getCategories', 'CategoryController@getCategories');
Route::post('category/{category}/insertBefore', 'CategoryController@insertBefore');
Route::post('category/{category}/insertAfter', 'CategoryController@insertAfter');
Route::post('category/{category}/moveUp', 'CategoryController@moveUp');
Route::post('category/{category}/moveDown', 'CategoryController@moveDown');
Route::resource('category', 'CategoryController');
